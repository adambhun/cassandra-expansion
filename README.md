# Expanding a Cassandra cluster from Alicloud to GCP

The guide written in Cassandra.odt explains how to expand a non-prouduction ready Cassandra cluster to have a data center in Alicloud and another in Google Cloud and configuring it to be production ready.

This is a risky, long and complicated process and it can result in erroneous data replication, so creating a temporary cluster with at least some of the data you have to test out everything is the safest way to do this.
As every platform and software involved is subject to change, make you have to make sure that you do everything perfectly on the first try with you production cluster.

Note: the shellscript commands may not work on every OS, because the type and version of the shell they use may be different from what I used, but they are simple enough and not crucial.

# Prerequisites

Networking: connection established between GCP and Alicloud. There are many options for this - for example a VPN - and comparing them would be out of the scope of this guide.

A project in GCP with IAM and networking set up.

Backup on Alicloud. Switching snitches can result in erroneous data replication.

# Useful links

This is a collection of resources I used to write this guide. Read these if something is not clear.

https://docs.datastax.com/en/cassandra-oss/3.0/cassandra/operations/opsAddDCToCluster.html
https://docs.datastax.com/en/cassandra-oss/3.0/cassandra/operations/opsSwitchSnitch.html
https://docs.datastax.com/en/cassandra-oss/3.0/cassandra/operations/opsChangeKSStrategy.html
http://adamhutson.com/cloning-cassandra-clusters-the-fast-way/
https://docs.datastax.com/en/cql-oss/3.3/cql/cql_reference/cqlshConsistency.html
https://www.baeldung.com/cassandra-cluster-datacenters-racks-nodes
https://docs.datastax.com/en/cassandra-oss/2.1/cassandra/configuration/configCassandra_yaml_r.html#configCassandra_yaml_r__initial_token